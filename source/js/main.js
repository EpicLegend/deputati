'use strict';

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js
//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js
//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/swiper/swiper-bundle.js

//= library/jquery.fancybox.min.js

//= components/sliders.js




$(document).ready(function () {


	// START btn-to-top
	let goTopBtn = document.querySelector('.btn-to-top');
	if( goTopBtn ) {
		window.addEventListener('scroll', trackScroll);
		goTopBtn.addEventListener('click', backToTop);
	}
	
	function trackScroll() {
		let scrolled = window.pageYOffset;

		if (scrolled > 500) {
			goTopBtn.classList.add('btn-to-top_show');
		}
		if (scrolled < 500) {
			goTopBtn.classList.remove('btn-to-top_show');
		}
	}

	function backToTop() {
		if (window.pageYOffset > 0) {
			window.scrollBy(0, -80);
			setTimeout(backToTop, 0);
		}
	}
	// END btn-to-top






	// BURGER
	$(".btn__menu").on("click", function (e) {
		this.classList.toggle("active");
		$("#modal-menu").toggleClass("show");
		$("body").toggleClass("modal-open");
	});
	$(".modal-menu__close").on("click", function() {
		$("#burger").removeClass("active");
		$("#modal-menu").removeClass("show");
		$("body").removeClass("modal-open");
	});
	$(window).resize(function(){
		if( $(window).width() > 991 ) {
			$("#burger").removeClass("active");
			$("#modal-menu").removeClass("show");
			$("body").removeClass("modal-open");
			$(".btn__menu").removeClass("active");
		}
	});
	
});

