$(document).ready(function () {

	let swiperWelcome = new Swiper('.slider-rf', {
		slidesPerView: 1,
		spaceBetween: 30,
		loop: true,
		lazy: true,
		navigation: {
			nextEl: '.swiper-button.slider-rf-next',
			prevEl: '.swiper-button.slider-rf-prev',
		},
		breakpoints: {
			640: {
			  slidesPerView: 2,
			},
			768: {
			  slidesPerView: 2,
			},
			1024: {
			  slidesPerView: 3,
			},
		}
	});
	  
});
